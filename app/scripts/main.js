$(document).ready(function() {

  $('.owl-license').owlCarousel({

    navigation: true,
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    items: 2,
    itemsDesktop: [1199, 2],
    itemsDesktopSmall: [979, 2],
    itemsDesktopSmall: [768, 1],
    itemsTablet: false,
    itemsMobile: false,
    navigationText: [
      '<img src=\'images/icon_owl_left.png\'>',
      '<img src=\'images/icon_owl_right.png\'>'
    ]

  });

});